package ferry.cedric.deloitte.utils

import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View

// Thanks to : https://stackoverflow.com/questions/26566954/square-layout-on-gridlayoutmanager-for-recyclerview
// Kotlin re-write
class GridSpacingItemDecoration(private val spanCount: Int,
                                private val spacingDp: Int = 8,
                                private val includeEdge: Boolean = false)
                                : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect,
                                view: View,
                                parent: RecyclerView,
                                state: RecyclerView.State) {
        val position = parent.getChildAdapterPosition(view) // item position
        val spacingPx = spacingDp.toPx()

        val column = position % spanCount // item column


        if (includeEdge) {
            // spacingDp - column * ((1f / spanCount) * spacingDp)
            outRect.left = spacingPx - column * spacingPx / spanCount
            // (column + 1) * ((1f / spanCount) * spacingDp)
            outRect.right = (column + 1) * spacingPx / spanCount

            if (position < spanCount) { // top edge
                outRect.top = spacingPx
            }
            outRect.bottom = spacingPx // item bottom
        } else {
            outRect.left = column * spacingPx / spanCount // column * ((1f / spanCount) * spacingDp)
            // spacingDp - (column + 1) * ((1f /    spanCount) * spacingDp)
            outRect.right = spacingPx - (column + 1) * spacingPx / spanCount
            if (position >= spanCount) {
                outRect.top = spacingPx // item top
            }
        }
    }
}