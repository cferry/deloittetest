package ferry.cedric.deloitte.utils

import android.databinding.BindingAdapter
import android.widget.ImageView
import com.squareup.picasso.Picasso
import ferry.cedric.deloitte.R

@BindingAdapter("imageUrl")
fun loadImage(view: ImageView, imageUrl: String) {
    //TODO: Ideally we should test if the URL is valid not just empty
    if(imageUrl.isNotEmpty()) {
        Picasso.get()
                .load(imageUrl)
                .placeholder(R.mipmap.ic_launcher_round)
                .into(view)
    } else {
        Picasso.get()
                .load(R.mipmap.ic_launcher_round)
                .placeholder(R.mipmap.ic_launcher_round)
                .into(view)
    }
}
