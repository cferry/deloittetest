package commsec.android.utils

import android.support.v7.widget.RecyclerView
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.StaggeredGridLayoutManager

// Thanks to https://gist.github.com/nesquena/d09dc68ff07e845cc622
abstract class InfiniteRecyclerViewScrollListener(val layoutManager: LinearLayoutManager,
                                                    // The minimum amount of items to have
                                                    // below your current scroll position
                                                    // before loading more.
                                                 private var visibleThreshold: Int = 5)
                                                    :RecyclerView.OnScrollListener() {


    // The current offset index of data you have loaded
    private var currentPage = 0

    // The total number of items in the dataset after the last load
    private var previousTotalItemCount = 0

    // True if we are still waiting for the last set of data to load.
    private var loading = true

    // Sets the starting page index
    private val startingPageIndex = 0


    init {
        if(layoutManager is GridLayoutManager) {
            visibleThreshold = visibleThreshold * layoutManager.spanCount
        }
    }

    fun getLastVisibleItem(): Int {
        if (layoutManager is StaggeredGridLayoutManager) {
            val lastVisibleItemPositions = layoutManager.findLastVisibleItemPositions(null)

            var maxSize = 0
            if(lastVisibleItemPositions != null) {
                for (i in lastVisibleItemPositions.indices) {
                    if (i == 0) {
                        maxSize = lastVisibleItemPositions[i]
                    } else if (lastVisibleItemPositions[i] > maxSize) {
                        maxSize = lastVisibleItemPositions[i]
                    }
                }
            }
            return maxSize

        } else if (layoutManager is LinearLayoutManager) {
            return layoutManager.findLastVisibleItemPosition()
        }

        return 0
    }


    // This happens many times a second during a scroll, so be wary of the code you place here.
    // We are given a few useful parameters to help us work out if we need to load some more data,
    // but first we check if we are waiting for the previous load to finish.
    override fun onScrolled(view: RecyclerView, dx: Int, dy: Int) {
        val lastVisibleItemPosition = getLastVisibleItem()
        val totalItemCount = layoutManager.itemCount

        // If the total item count is zero and the previous isn't, assume the
        // list is invalidated and should be reset back to initial state
        if (totalItemCount < previousTotalItemCount) {
            resetState()
        }

        // If it’s still loading, we check to see if the dataset count has
        // changed, if so we conclude it has finished loading and update the current page
        // number and total item count.
        if (loading && totalItemCount > previousTotalItemCount) {
            loading = false
            previousTotalItemCount = totalItemCount
        }

        // If it isn’t currently loading, we check to see if we have breached
        // the visibleThreshold and need to reload more data.
        // If we do need to reload some more data, we execute onLoadMore to fetch the data.
        // threshold should reflect how many total columns there are too
        if (!loading && lastVisibleItemPosition + visibleThreshold > totalItemCount) {
            currentPage += 1
            onLoadMore(currentPage, totalItemCount, view)
            loading = true
        }
    }

    // Call this method whenever performing new searches
    fun resetState(force: Boolean = false) {
        this.currentPage = this.startingPageIndex

        if(force) {
            this.previousTotalItemCount = layoutManager.itemCount
        } else {
            this.previousTotalItemCount = 0
        }
        // Set loading flag
        if (force || layoutManager.itemCount == 0) {
            this.loading = true
        }
    }

    // Defines the process for actually loading more data based on page
    abstract fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView)

}