package ferry.cedric.deloitte

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData

import android.arch.lifecycle.*
import ferry.cedric.data.FlickrPhoto

import ferry.cedric.data.FlickrRepository
import ferry.cedric.data.FlickrResponse

class MainViewModel(application: Application): AndroidViewModel(application) {

    // could be used to implement a search bar
    val searchString = "kittens"

    var page = 0

    var maxPage = 0

    // Save the current list of Photos
    private var listPhotos : ArrayList<FlickrPhoto> = ArrayList()

    // photo list to be observed by fragment/activity
    val mediator = MediatorLiveData<ArrayList<FlickrPhoto>>()

    // error state to be observed by fragment/activity
    val hasError: MutableLiveData<Boolean> = MutableLiveData()

    init {
        hasError.postValue(false)

        loadMore()
    }

    /**
     * extract required infos from FlickR Response Object
     */
    private fun transformUpdate(flickrResponse: FlickrResponse?) {

        if(flickrResponse != null) {
            maxPage = flickrResponse.photos.pages
            listPhotos.addAll(flickrResponse?.photos?.photo)
        } else {
            hasError.postValue(true)
        }

    }

    /**
     * exposed to allow activity/fragment to request for more
     */
    fun loadMore() {
        if (page <= maxPage) {
            mediator.addSource(FlickrRepository().getPhotos(searchString, page), Observer { flickrResponse ->
                transformUpdate(flickrResponse)
                mediator.postValue(listPhotos)
            })

            page += 1
        } else {
            hasError.postValue(true)
        }
    }
}