package ferry.cedric.deloitte

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar

import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import commsec.android.utils.InfiniteRecyclerViewScrollListener
import ferry.cedric.deloitte.databinding.ActivityMainBinding
import ferry.cedric.deloitte.utils.GridSpacingItemDecoration

class MainActivity : AppCompatActivity() {

    private var viewModel: MainViewModel? = null
    private var binding: ActivityMainBinding? = null
    private val recyclerViewAdapter = FlickrPhotoAdapter(arrayListOf())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)


        val photosRecyclerView = findViewById<RecyclerView>(R.id.recycler_view)
        photosRecyclerView.addItemDecoration(GridSpacingItemDecoration(3))

        val infiniteScroll = object: InfiniteRecyclerViewScrollListener(photosRecyclerView.layoutManager as GridLayoutManager, 7) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView) {
                viewModel?.loadMore()
            }
        }

        photosRecyclerView.addOnScrollListener(infiniteScroll)

        photosRecyclerView.adapter = recyclerViewAdapter

        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)


        binding?.mainViewModel = viewModel

        viewModel?.mediator?.observe(this, Observer { photos ->
            if(photos != null) {
                recyclerViewAdapter.addPhotos(photos)
            }
        })

        viewModel?.hasError?.observe(this, Observer { error ->
            if(error ?: false) {
                Snackbar.make(photosRecyclerView, R.string.error, Snackbar.LENGTH_LONG).show()
            }
        })
    }
}
