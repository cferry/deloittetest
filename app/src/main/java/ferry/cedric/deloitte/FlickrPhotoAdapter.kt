package ferry.cedric.deloitte

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import ferry.cedric.data.FlickrPhoto
import ferry.cedric.deloitte.databinding.PhotoItemBinding

class FlickrPhotoAdapter(private var listPhotos: ArrayList<FlickrPhoto>) : RecyclerView.Adapter<FlickrPhotoAdapter.RecyclerViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {

        val itemPhotoBinding: PhotoItemBinding = DataBindingUtil.inflate(
                            LayoutInflater.from(parent.context),
                            R.layout.photo_item,
                            parent,
                            false)

        return RecyclerViewHolder(itemPhotoBinding)
    }

    override fun getItemCount(): Int = listPhotos.size

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        var currentPhoto: FlickrPhoto = listPhotos[position]
        holder.bind(currentPhoto)
    }

    fun addPhotos(listPhotos: List<FlickrPhoto>) {
        this.listPhotos.clear()
        this.listPhotos.addAll(listPhotos)
        notifyDataSetChanged()
    }

    class RecyclerViewHolder(val binding: PhotoItemBinding) : RecyclerView.ViewHolder(binding.getRoot()) {
        fun bind(photoItem: FlickrPhoto) {
            binding.setVariable(BR.photoItem, photoItem);
            binding.executePendingBindings()
        }
    }
}