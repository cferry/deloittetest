package ferry.cedric.data

import org.junit.Test

import org.junit.Assert.*;

class FlickrPhotoTest {

    @Test
    fun getURL_isCorrect() {
        val flickPhoto = FlickrPhoto("12345678",  "Image 1", 5, "australia", "secret")

        assertEquals("Ensure URL is generated properly", "http://farm5.static.flickr.com/australia/12345678_secret_s.jpg", flickPhoto.getUrl())
    }
}