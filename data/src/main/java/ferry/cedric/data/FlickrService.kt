package ferry.cedric.data

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query


interface FlickrService {

    // https://www.flickr.com/services/api/flickr.photos.search.html

    @GET("services/rest/?method=flickr.photos.search" +
            "&format=json&nojsoncallback=1")
    fun getPhotos(@Query("text") text: String = "kittens",
                  @Query("api_key") api_key: String = "96358825614a5d3b1a1c3fd87fca2b47",
                  @Query("page") page: Int = 0)
            : Call<FlickrResponse>

}
