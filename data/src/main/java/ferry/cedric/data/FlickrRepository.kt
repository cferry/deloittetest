package ferry.cedric.data

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class FlickrRepository {
    private val apiService: FlickrService

    init {
        var retrofit = Retrofit.Builder()
                .baseUrl("https://api.flickr.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        apiService = retrofit.create<FlickrService>(FlickrService::class.java)
    }

    fun getPhotos(search: String, page: Int = 0): MutableLiveData<FlickrResponse> {

        val data = MutableLiveData<FlickrResponse>()

        apiService.getPhotos(text = search, page = page)
                .enqueue(object : Callback<FlickrResponse> {
                    override fun onResponse(call: Call<FlickrResponse>, response: Response<FlickrResponse>) {
                        if (response.isSuccessful()) {
                            data.setValue(response.body())
                        }
                    }

                    override fun onFailure(call: Call<FlickrResponse>, t: Throwable) {
                        data.setValue(null)
                    }
                })
        return data

    }
}