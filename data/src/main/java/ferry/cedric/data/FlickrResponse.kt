package ferry.cedric.data

/*
{
    photos: {
        page: 1,
        pages: 2015,
        perpage: 100,
        total: "201485",
        photo: [
            {
            id: "44468257451",
            owner: "134066368@N07",
            secret: "42e987be40",
            server: "1895",
            farm: 2,
            title: "IMG_1470",
            ispublic: 1,
            isfriend: 0,
            isfamily: 0
            },
        ]
        },
        stat: "ok
}
*/

data class FlickrResponse(val photos: FlickrPhotos)

data class FlickrPhotos(val page: Int, val pages: Int, val perpage: Int, val total: String, val photo: ArrayList<FlickrPhoto>)

