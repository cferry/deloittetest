package ferry.cedric.data

data class FlickrPhoto(val id: String, val title: String, val farm: Int, val server: String, val secret: String) {
    fun getUrl():String {
        // https://www.flickr.com/services/api/misc.urls.html
        return "http://farm${farm}.static.flickr.com/${server}/${id}_${secret}_s.jpg"
    }
}