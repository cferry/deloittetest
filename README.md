# Deloitte Test

## Requirements
 * Android Studio 3.1.x
 * Internet connection

## Brief
Create an Android App that display a grid of photo from FlickR API.

## Technical choices
### Retrofit with GSON
[Retrofit](https://square.github.io/retrofit/) Retrofit turns HTTP API into a java/kotlin interface, combined with [GSON]() it helps to map the JSON Object into Java/Kotlin Object. It also handle network  asynchronousness offering a system of callbacks.
It is use here to fetch flickr photo list and map it into a Kotlin object.

### LiveData
[LiveData](https://developer.android.com/topic/libraries/architecture/livedata) is an observable data holder class. Unlike a regular observable, LiveData is lifecycle-aware, meaning it respects the lifecycle of other app components, such as activities, fragments, or services. This awareness ensures LiveData only updates app component observers that are in an active lifecycle state.
LiveData is used to notify any changes from the Retrofit/Network call to the ViewModel but also to the UI (Activity)

### MVVM Model, View, ViewModel architecture
[MVVM](https://en.wikipedia.org/wiki/Model–view–viewmodel) facilitates a separation of development of the graphical user interface, from development of the business logic or back-end logic (the data model).
MVVM has been implemented here, thanks to LiveData, data can flow from the network to the view thanks to the viewmodel.

### Databindings
[Databindings](https://developer.android.com/topic/libraries/data-binding/) allow binding UI Components in the layout to the data source, using declarative format. It prevent having to map layout into the activity for instance.
It is used in conjunction with LiveData, in order to fill-up layouts with data.

### Picasso
[Picasso](http://square.github.io/picasso/) allows for hassle-free image loading in your application. Combined with a Binding adapter, it's the beautiful way to load flickr photos asynchroniously.

## Tests
A very light test coverage is in place here, mostly because of time constraints.

## Features
 * The app load data from remote API thanks to Retrofit
 * In case of error, a Snackbar is displayed at the bottom of the screen
 * App is responsive (can scroll, rotate) while updating
 * Infinite scrolling is implemented

## Limitation
 * No progress bar has been implemented


